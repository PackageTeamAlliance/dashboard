<?php

$router->group(['namespace' => 'Pta\Dashboard\Http\Controllers\Admin'], function () use ($router) {
    
    $router->get('/', ['as' => 'dashboard.index', 'uses' => 'DashboardController@index']);
    
});