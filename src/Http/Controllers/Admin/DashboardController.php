<?php

namespace Pta\Dashboard\Http\Controllers\Admin;

use Pta\Dashboard\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function index()
    {
        return $this->getView('admin.dashboard.index');
    }
}
