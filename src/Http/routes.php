<?php

Route::group(['namespace' => 'Pta\Dashboard\Http\Controllers\Admin'], function()
{
	Route::get('/', ['as' => 'dashboard.index', 'uses' => 'DashboardController@index']);
});