<?php

namespace Pta\Dashboard\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

abstract class Request extends FormRequest
{
    protected function checkPermissions($request, $roles, $any) {
        
        return true;
        
        if (is_null($this->user)) {
            return false;
        }
        
        if (is_null($roles)) {
            $roles = $this->getRoles($request);
            $anyRole = $this->getAny($request);
        } 
        else {
            $roles = explode('|', $roles);
            $anyRole = $any;
        }
        
        if (is_array($roles) and count($roles) > 0) {
            $hasResult = true;
            
            foreach ($roles as $role) {
                
                if (in_array($role, $request->session()->get('roles'))) {
                    $hasRole = true;
                } 
                else {
                    $hasRole = false;
                }
                
                // Check if any role is enough
                if ($anyRole and $hasRole) {
                    return true;
                }
                
                $hasResult = $hasResult & $hasRole;
            }
            
            if (!$hasResult) {
                
                return false;
            }
        }
        
        return false;
    }
    
    /**
     * @param $request
     *
     * @return mixed
     */
    protected function getAny($request) {
        $routeActions = $this->getActions($request);
        
        return array_get($routeActions, 'any', false);
    }
    
    /**
     * @param $request
     *
     * @return mixed
     */
    protected function getActions($request) {
        $routeActions = $request->route()->getAction();
        
        return $routeActions;
    }
}
