<?php
namespace Pta\Dashboard\Providers;

use File;
use Illuminate\Support\ServiceProvider;

class DashboardServiceProvider extends ServiceProvider
{
    
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;
    
    /**
     * Providers to register
     *
     * @var array
     */
    protected $providers = [];
    
    public function boot() {
        
        $this->publishes([ __DIR__ . '/../../config/config.php' => config_path('dashboard.php'), 'config']);

        $this->registerBindings();
        
        $this->registerViews();
        
        $this->populateMenus();
        
        $this->registerTranslations();
        
        $this->registerMigration();
        
        $this->registerRoutes();
        
    }
    
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register() {
        
        foreach ($this->providers as $provider) {
            
            $this->app->register($provider);
        }
        
        $this->registerConfig();
    }
    
    /**
     * Register Bindings in IoC.
     *
     * @return void
     */
    protected function registerBindings() {
    }
    
    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig() {

        $this->mergeConfigFrom(__DIR__ . '/../../config/config.php', 'dashboard');
    }
    
    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews() {
        
        $this->loadViewsFrom(realpath(__DIR__ . '/../../resources/views'), 'pta/dashboard');
        
        $this->publishes([realpath(__DIR__ . '/../../resources/views') => base_path('resources/views/vendor/pta/dashboard'), ]);
    }
    
    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations() {
        
        $this->loadTranslationsFrom(realpath(__DIR__ . '/../../resources/lang'), 'pta/dashboard');
    }
    
    public function registerMigration() {
        
        $this->publishes([realpath(__DIR__ . '/../../database/migrations') => database_path('/migrations') ], 'migrations');
    }
    
    public function populateMenus() {
    }
    
    public function registerRoutes() {
        
        $router = $this->app['router'];
        
        $prefix = config('dashboard.route_prefix');
        
        $security = config('dashboard.security.protected');

        
        if (!$this->app->routesAreCached()) {
            
            $group = [];
            
            $group['prefix'] = $prefix;
            
            if ($security) {
                
                $middleware =  config('dashboard.security.middleware', ['auth', 'needsPermission']);
                $permissions = config('dashboard.security.permission_name');
                
                $group['middleware'] = config('dashboard.security.middleware', $middleware);
                $group['can'] =        config('dashboard.security.permission_name', $permissions);
            }
            
            $router->group($group, function () use ($router) {
                
                require realpath(__DIR__ . '/../routes.php');
            });
        }
    }
    
    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides() {
        return array();
    }
}
