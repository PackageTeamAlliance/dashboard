<?php

namespace Pta\Dashboard\Console\Installers;

use Illuminate\Console\Command;
use Pta\Pulse\Console\Contracts\Installer;

class DashboardInstaller implements Installer
{
    
    /**
     * @var Command
     */
    protected $command;
    
    public function run(Command $command) {
     
     	$this->command = $command;

     	$this->command->call('module:migrate', ['module' => 'dashboard']);

        $this->command->call('module:seed', ['module' => 'dashboard']);
    }
}
