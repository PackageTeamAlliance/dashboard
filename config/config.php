<?php

return [

    'name' => 'Dashboard',
    /*
     * Default prefix to the dashboard.
     */
    'route_prefix' => config('core.admin_uri'),
    /*
     * Default permission user should have to access the dashboard.
     */
    'security' => [
        'protected' => true,
        'middleware' => ['auth', 'admin.check'],
        'permission_name' => 'dashboard.dashboard.manage',
    ],
    'permissions' => [
        [
            'name' => 'dashboard.dashboard.manage',
            'readable_name' => 'Manage Dashboard',
        ]
    ],
    'navigation' => [
        [
            'menu_id' => -1,
            'order' => 0,
            'dropdown' => 0,
            'submenu' => 0,
            'active' => 1,
            'name' => 'Dashboard',
            'icon' => '',
            'route' => 'dashboard.index',
            'title' => 'Dashboard',
            'roles' => -1,
            'children' => []
        ]
    ],
    /*
     * Default url used to redirect user to front/admin of your the system.
     */
    'system_url' => config('core.redirect_url'),

];
