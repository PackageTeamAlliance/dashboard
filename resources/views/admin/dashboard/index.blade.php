@extends('admin.layouts.master')

@section('content')

	<div class="panel panel-inverse">

        <div class="panel-heading">
            <h4 class="panel-title">Hello world</h4>
        </div>

        <div class="panel-body">
            <h4 class="m-t-0">Laravel 5</h4>
            <div class="quote">This view is loaded from module: {!! config('pta/dashboard.name') !!}</div>
        </div>
    </div>

@stop