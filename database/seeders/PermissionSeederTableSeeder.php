<?php

namespace Modules\Dashboard\Database\Seeders;

use DB;
use Guardian;
use DateTime;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class PermissionSeederTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $permissions = config('dashboard.permissions');

        $admin_role = config('core.admin_role');

        if (!$role = Guardian::findRole($admin_role)) {
            $role = Guardian::createRole($admin_role);
        }

        foreach ($permissions as $permission) {

            $name = $permission['name'];
            $readable_name = $permission['readable_name'];
            if (!$perm = DB::table('permissions')->where('name', '=', $name)->first()) {
                $perm = DB::table('permissions')->insert([
                    'name' => $name,
                    'readable_name' => $readable_name,
                    'created_at' => new DateTime,
                    'updated_at' => new DateTime
                ]);
            }
            $permission = Guardian::findPermission($name);

            $role->permissions()->attach($permission, ['value' => -1]);
        }
    }
}
