<?php

namespace Modules\Dashboard\Database\Seeders;

use DB;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Dashboard\Database\Seeders\PermissionSeederTableSeeder;

class DashboardDatabaseSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin_nav = config('core.admin_nav.name');

        if (!$menu = DB::table('menus')->where('name', $admin_nav)->first()) {
            $menu = DB::table('menus')->insert([
                'name' => $admin_nav,
                'presenter' => config('core.admin_nav.presenter'),
                'module_name' => config('core.admin_nav.module_name')
            ]);
        }

        $navigation = config('dashboard.navigation');

        foreach ($navigation as $nav) {

            $nav['menu_id'] = $menu->id;
            $nav['roles'] = '"' . config('core.admin_role') . '"';
            $children = $nav['children'];

            unset($nav['children']);

            if (!$menu_field = DB::table('menu_fields')->where('name', $nav['name'])->first()) {
                DB::table('menu_fields')->insert($nav);
                $menu_field = DB::table('menu_fields')->where('name', $nav['name'])->first();

                foreach ($children as $child) {
                    $this->createLink($child, $menu->id, $menu_field->id);
                }
            }

            \Artisan::call('cache:clear');

        }

        $this->call(PermissionSeederTableSeeder::class);

    }

    public function createLink($child, $menu_id, $parent_id)
    {
        $child['menu_id'] = $menu_id;
        $child['parent'] = $parent_id;
        $child['roles'] = '"' . config('core.admin_role') . '"';
        if (!DB::table('menu_fields')->where('name', $child['name'])->first()) {
            DB::table('menu_fields')->insert($child);
        }
    }

}
